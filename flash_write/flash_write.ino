/******************************************************
 *日期：2022.1.7 
 *作者：iron2222
 *程序目标：此程序用于演示如何向NodeMCU的SPIFFS中建立名为
 *        notes.txt的文件，程序还将向该文件写入信息。
 *函数说明：
 *SPIFFS.open(file_name, "w"); 
 *以上函数有两个参数：
 *第一个参数是被操作的文件名称，本示例中该文件为/notes.txt
 *第二个参数"w" 代表写入文件信息。
 ******************************************************/
#include <FS.h>

String file_name = "/iron2222/notes.txt"; //被读取的文件位置和名称

void setup() {
  Serial.begin(9600);
  Serial.println("");
  
  Serial.println("SPIFFS format start");
  SPIFFS.format();    // 格式化SPIFFS
  Serial.println("SPIFFS format finish");
  
  if(SPIFFS.begin()){ // 启动SPIFFS
    Serial.println("SPIFFS Started.");
  } else {
    Serial.println("SPIFFS Failed to Start.");
  }

  File dataFile = SPIFFS.open(file_name, "w");// 建立File对象用于向SPIFFS中的file对象（即/notes.txt）写入信息
  dataFile.println("Hello IOT World.");       // 向dataFile写入字符串信息
  dataFile.close();                           // 完成文件写入后关闭文件
  Serial.println("Finished Writing data to SPIFFS");


}

void loop() {
  // put your main code here, to run repeatedly:

}
