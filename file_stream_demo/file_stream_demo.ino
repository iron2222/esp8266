
/**********************************************************************
时间：2022.1.9     
作者：iron2222
程序目的：
通过File对象演示Stream操作
***********************************************************************/
#include <FS.h>  
 
String file_name = "/iron2222/notes.txt"; //被读取的文件位置和名称
 
void setup() {
  Serial.begin(9600);
  Serial.println("");
 
  // 启动SPIFFS
  if(SPIFFS.begin()){ 
    Serial.println("SPIFFS Started.");
  } else {
    Serial.println("SPIFFS Failed to Start.");
  }
 
  File dataFile = SPIFFS.open(file_name, "w");// 建立File对象用于向SPIFFS中的file对象（即/notes.txt）写入信息
  dataFile.println("Hello IOT World.");       // 向dataFile写入字符串信息
  dataFile.close();                           // 完成文件写入后关闭文件
  Serial.println("Finished Writing data to SPIFFS");
 
  // 使用find函数从dataFile中找到指定信息
  Serial.print("Try to find IOT in ");Serial.println(file_name); 
   
  dataFile = SPIFFS.open(file_name, "r");     // 以“r”模式再次打开闪存文件
  if (dataFile.find("IOT")){                  // 在闪存文件中查找文字"IOT"
    Serial.print("Found IOT in file: ");      // 如果找到则告知用户找到文字"IOT"
    Serial.println(file_name);         
  }
 
  // 使用readString读取执行完find函数后的dataFile内容并显示与串口监视器
  Serial.println("Use readString to get contents of dataFile after find");
  Serial.println(dataFile.readString());
    
  dataFile.close();                           // 完成操作后关闭文件  
}
 
void loop() {}
