
/**********************************************************************
时间：2022.1.9     
作者：iron2222
程序目的：
此程序使用Serial库来演示Stream类中的parseInt函数的使用方法。parseInt函数可用于从设备
接收到的数据中寻找整数数值。当您把本程序上传后，可通过串口监视器进行以下测试。
 
实验：输入信息：
k123oktaichi678maker567
设备输出信息:
Please enter input...
Found ok in user input.
serialParseInt = 678
serialInput = maker567
***********************************************************************/
void setup() {
  Serial.begin(9600);
  Serial.println("");
  Serial.println("Please enter input...");
}
 
void loop() {
  while(Serial.available()){
    if(Serial.find("ok")){
      Serial.println("Found ok in user input.");
 
      int serialParseInt = Serial.parseInt();
      Serial.print("serialParseInt = ");
      Serial.println(serialParseInt);
      
      String serialInput = Serial.readString();
      Serial.print("serialInput = ");
      Serial.println(serialInput);
    }
  }
}
